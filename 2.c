#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>


const int SIZE = 200;

void get_time(char *into_string) {
    time_t now_time;
    time(&now_time);
    struct tm *now_tm = localtime(&now_time);
    struct timespec now_timespec;
    clock_gettime(CLOCK_MONOTONIC, &now_timespec);
    long milliseconds = now_timespec.tv_nsec / 1000000;

    snprintf(into_string, SIZE, "%d:%d:%d:%ld", now_tm->tm_hour, now_tm->tm_min, now_tm->tm_sec, milliseconds);
}

void print_pid(int flag) {
    pid_t pid = getpid();
    char time_string[SIZE];
    if (pid != flag) {
        get_time(time_string);
        printf("This is a child pid: %d, its parent's id: %d, time: %s\n", getpid(), getppid(), time_string);
    } else if (pid == flag) {
        get_time(time_string);
        printf("This is a parent pid: %d, time: %s\n", getpid(), time_string);
    } else {
        printf("An error occurred\n");
    }
}

int main() {
    int flag = getpid();
    pid_t pid1 = fork();
    pid_t pid2;

    if (pid1 > 0) {
        pid2 = fork();
    }

    print_pid(flag);

    if (pid1 > 0 && pid2 > 0) {
        printf("\n Running the command 'ps -x'\n");
        system("ps -x");
    }

    return 0;
}
